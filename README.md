# M5StickC Plus and M5StickC Plus 2 PORTAL.HACK Firmware

Firmware for advanced security tests on M5StickC Plus and M5StickC Plus 2 ESP32 Devices

✅Purchase the full version for compatibility with all devices.

## BUY COMPLETE FIRMWARE ⬇️

✅**SHOP:** https://masterbudz01.mysellix.io/it/product/portal-hack-device-firmware

🚨**Important:** Click on this link or scroll down for the link to purchase the complete firmware with all described functions.

![PORTAL.HACK Logo](https://imagedelivery.net/95QNzrEeP7RU5l5WdbyrKw/9d3acc2b-966d-40d0-170c-30c51cb65a00/shopitem)

![PORTAL.HACK Device Pins](https://imagedelivery.net/95QNzrEeP7RU5l5WdbyrKw/d2b3e0c6-1964-4b55-1427-9ecd7f2e8300/shopitem)

![PORTAL.HACK Device Pins2](https://i.ibb.co/dBqk89P/recivepinout.png)

FIRMWARE BY @MASTERBUDZ_REAL

FIRMWARE VIDEOS:
https://www.tiktok.com/@master.portalhack

## PORTAL.HACK FIRMWARE FOR M5StickC Plus and M5StickC Plus 2
PORTAL.HACK is an intriguing firmware project developed for M5StickC Plus and M5StickC Plus 2, created to explore the capabilities of ESP32 development with the Arduino IDE. This firmware incorporates various advanced features for educational and experimental purposes.

## Features:

- **IR-BRUTE**: Performs brute-force attacks on infrared remote control systems. Useful for identifying the correct code needed to control IR devices like TVs or air conditioners.

- **BLE-HACK**: Enables hacking of Bluetooth Low Energy (BLE) devices. This may allow unauthorized access to devices such as wireless headphones, smartwatches, or IoT devices.

- **WIFI-HACK**: Conducts hacking attacks against WiFi networks, exploiting vulnerabilities or weaknesses in security protocols. Can be used to attack WiFi networks.

- **BLE SCAN**: Scans nearby Bluetooth Low Energy (BLE) networks, displaying available BLE devices and their characteristics, such as wearable devices or Bluetooth beacons.

- **TAMAGOTCHI**: A virtual pet simulation experience on the device's display, reminiscent of the popular '90s Tamagotchi.

- **BLE BAD-USB**: Utilizes a combination of Bluetooth and USB to execute malicious code from target devices. Can be used for "BadUSB" attacks via Bluetooth connectivity.

- **WEB LINKS**: Opens embedded hyperlinks (URLs). Useful for quickly accessing online hacking resources..

- **SOUND NOISE**: Emits an annoying sound, which could be used to disrupt or annoy people nearby. May have applications in pranks or as a deterrent in certain situations.

- **LED LIGHT**: Controls the turning on and off of an LED. Can be used for visual effects or as a visual indicator in specific scenarios.

- **RF SIGNAL JAM**: Disrupts radio frequency (RF) signals nearby, potentially interfering with wireless communications. Can be used for security purposes or experimental purposes.

- **RF SCANNER**: Scans radio frequencies, displaying detected signals on the device's display. Useful for analyzing and monitoring RF activity in the surrounding environment.

- **RF GRABBER**: Captures and stores radio signals for analysis or replication. Can be used to study RF signal behavior or reproduce them later.

- **RF REPLY**: Replicates previously recorded radio signals. Useful for playback of captured RF signals for testing or demonstrative purposes.

- **RF RECORD**: Records detected radio signals for further analysis. Useful for storing and analyzing RF signals for security or experimental purposes.

- **RF REC-REPLY**: Records and replays detected radio signals. Useful for capturing and replaying RF signals for testing or demonstrative purposes.

- **RF COPY-RAW**: Copies radio signals in raw binary format. Useful for detailed analysis of RF signals and manipulation of raw data.

- **RF SEND-RAW**: Transmits radio signals in raw binary format. Useful for transmitting custom RF signals or for specific testing purposes.

- **RF FILE-SAVE**: Saves radio signals to files for storage or analysis. Useful for preserving captured RF data for future reference or signal analysis.

- **RF FILE-REPLY**: Loads and replays radio signals from files. Useful for playing back archived RF signals for testing or demonstration.

- **RF DEVICE BRUTE**: Conducts brute-force attacks against RF devices to identify correct access codes. Useful for testing the security of RF systems.

- **RF LIGHTS BRUTE**: Conducts brute-force attacks against RF light control systems. Useful for testing the security of lighting control systems operated via RF signals.

- **PARKING GATE BRUTE**: Conducts brute-force attacks against parking gate control systems. Useful for testing the security of parking access control systems.

- **CEILING FAN BRUTE**: Conducts brute-force attacks against ceiling fan control systems. Useful for testing the security of ceiling fan control systems operated via RF signals.

- **TESLA PORT OPEN**: Opens Tesla charging port doors. Can be used for testing purposes or to open Tesla charging port doors in specific situations.

- **DABRUIJN BRUTE**: Conducts brute-force attacks using the De Bruijn algorithm. Useful for testing the security of access systems utilizing the De Bruijn algorithm.

- **CAME 12bit BRUTE**: Conducts brute-force attacks against CAME systems with 12-bit keys. Useful for testing the security of access systems using 12-bit keys.

- **CAME 24bit BRUTE**: Conducts brute-force attacks against CAME systems with 24-bit keys. Useful for testing the security of access systems using 24-bit keys.

- **LINEAR 10bit BRUTE**: Conducts brute-force attacks against linear systems with 10-bit resolution. Useful for testing the security of linear access systems.

- **PRINCETON 24bit BR**: Conducts brute-force attacks against Princeton systems with 24-bit keys. Useful for testing the security of access systems using Princeton standards.

- **UNILARM 25bit BR**: Conducts brute-force attacks against systems with 25-bit data format for unilateral alarming or signaling purposes. Ideal for testing the security of alarm or signaling systems.

- **RF ROLLWAVES**: Generates random radio waves to interfere with wireless communications. Can be used for security purposes or experimental purposes.

- **RF AUDIO JAM**: Emits audio signals in radio frequencies to potentially interfere with communications. Can be used for disturbance or protection purposes.

- **NFC SCANNER**: Scans nearby NFC cards, displaying UID details. Useful for identification and verification of NFC cards.

- **NFC DOOR BRUTE**: Conducts brute-force attacks against NFC door access systems. Useful for testing the security of NFC access systems.

- **RFID FASTBRUTE**: Conducts rapid brute-force attacks against RFID systems. Useful for testing the security of RFID systems.

- **RFID SLOWBRUTE**: Conducts slow brute-force attacks against RFID systems. Useful for testing the security of RFID systems.

- **MIFARE UNLOCK**: Unlocks Mifare RFID systems. Can be used to access Mifare RFID systems in specific situations.

- **COPY RFID**: Copies RFID signals. Useful for cloning RFID cards for testing or demonstration purposes.

- **REPLAY RFID**: Replicates RFID signals. Useful for replaying captured RFID signals for testing or demonstration purposes.

- **TRAFFIC LIGHTS SET**: Manipulates infrared signals to set traffic lights to green. Can be used to change traffic light signals in specific situations.

- **CAM IR LIGHT-MASK**: Emits infrared illumination to mask facial recognition. Can be used for privacy or to avoid facial recognition.

- **DETECT SKIMMERS**: Detects skimmer devices for credit card data theft. Useful for identifying and protecting against skimmer devices.

- **DETECT PWNAGOTCHI**: Detects Pwnagotchi devices used for cyber attacks. Useful for identifying and protecting against Pwnagotchi devices.

- **DETECT FLIPPERZERO**: Detects FlipperZero devices used for cyber attacks. Useful for identifying and protecting against FlipperZero devices.

- **GPS INFO[DEV]**: Provides information on the device's GPS coordinates. Useful for obtaining information on the device's location.

*Note: These functionalities may interact with various devices, including gates. Please use responsibly and in compliance with applicable laws and regulations.*

## User Interface
Three main controls:
* **Home:** Stops the current process and returns to the menu.
* **Next:** Moves the cursor to the next menu option. In function modes, it stops the process and returns to the previous menu.
* **Select:** Activates the currently-selected menu option and wakes up the dimmed screen in function modes.

**Controls:**
* **Power:** Long-press the power button for 6 seconds to turn off the unit.
* **Home:** Tap the power button.
* **Next:** Tap the side button.
* **Select:** Tap the M5 button on the front of the unit.

## PORTAL.HACK Portal
In PORTAL.HACK Portal mode, an open WiFi Hotspot named "Vodafone Free WiFi" (configurable by clone another AP) is activated. It serves a fake login page for social engineering attacks, capturing entered usernames, passwords, emails, and OTPs. Cloning existing SSIDs is possible from the WiFi Scan details. 
Captured credentials can be viewed by connecting to the portal and browsing to http://172.0.0.1/creds. Custom SSIDs and settings can be configured in a similar manner. 
SD Card support logs usernames and passwords to portal-hack-creds.txt.

*Note: PORTAL.HACK is intended for professional engagements, education, or demonstration purposes only. Unauthorized use of personal information is against the law.*

*This project is in beta stage, and functionalities may improve over time. Updates and enhancements to the functionalities are expected to be implemented in future releases. Your feedback and contributions are greatly appreciated to help improve and expand the project's capabilities.*

## Installation of -DEMO- for M5StickC Plus
The firmware can be installed using WEB ESP-FLASHER for a hassle-free experience:
1. Launch https://budzminer.000webhostapp.com/
2. Select Correct "COM" port from the menu.
3. CLICK FLASH
4. Enjoy the firmware!

🚨Important Note: The DEMO firmware is compatible only with M5StickC Plus.
        Purchase the full version for compatibility with M5StickC Plus & M5StickC Plus 2.

## BUY COMPLETE FIRMWARE:

**Buy complete firmware with all described functions here:**

https://masterbudz01.mysellix.io/it/product/portal-hack-device-firmware

✅ Purchase this version for compatibility with all devices. ✅

**Flash DEMO Here:**

 *Web Flasher:* 
 - https://budzminer.000webhostapp.com/

🚨Important Note: The DEMO firmware is compatible only with M5StickC Plus.
        Purchase the full version for compatibility with all devices.

        This DEMO is only for testing some BLE and WIFI functions, 
        the FULL firmware is completely different from this demo 
        and is complete with all the functions indicated above.

**Where you can buy the device:**

https://www.aliexpress.com/item/1005006371086676.html

**Where you can buy additional modules:**

https://www.aliexpress.com/item/1005003328002232.html



![discord1](https://icons.iconarchive.com/icons/iconoir-team/iconoir/128/discord-icon.png) **Discord Comunity:** https://discord.gg/aJQE8bXgaj ![discord1](https://icons.iconarchive.com/icons/iconoir-team/iconoir/128/discord-icon.png)



**Additional Modules:**

![FS1000A Transmitter and Receiver Kit](https://5.imimg.com/data5/SELLER/Default/2023/12/369137165/HQ/NS/HL/14495069/fs1000a-433m-transmitter-and-receiver-kit.jpg)


![Home Assistant Community](https://community-assets.home-assistant.io/original/3X/c/9/c9bca4c8b6bbf084cb3b7bd568198f4b3d4a9d75.jpg)


![M5 Stack Module](https://www.mouser.it/images/marketingid/2021/img/121460232.png?v=070223.0329)


![nfc](https://isehara-3lv.sakura.ne.jp/blog/wp-content/uploads/2023/02/IMG_2103-scaled.jpeg)
